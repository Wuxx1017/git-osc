---
layout: post
title: "Gitee 上线 Pull Request 代码已阅功能，优雅合并多文件 PR"
---

<p><!-- wp:paragraph -->在开发者进行开源贡献或者所在的研发团队使用 Gitee 进行项目研发时，对代码仓库进行 Pull Request 是很常见的操作。&nbsp;</p>

<p><!-- /wp:paragraph --><!-- wp:paragraph --></p>

<p>Pull Request 大大提高了开发者们的协作开发效率，也让修改代码的操作变得格外方便。</p>

<p><!-- /wp:paragraph --><!-- wp:paragraph --></p>

<p>但当开发者收到含有多个文件更改的 PR 时，可能会有些意外的情况出现，比如忘记了哪个文件的更改是可以合并的，或是突然有事情打断，回过头来想不起来刚刚浏览到哪里，这就会让合并 PR 的效率大大降低。</p>

<p><!-- /wp:paragraph --><!-- wp:paragraph --></p>

<p>针对这种情况，Gitee 推出了<strong>&nbsp;Pull Request 代码已阅功能，</strong>Pull Request 已阅功能可以为用户保存 Pull Request 变更代码的查看进度，有效分辨已查看和未查看的代码。</p>

<p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --></p>

<h3><strong>如何使用</strong></h3>

<p><!-- /wp:heading --><!-- wp:paragraph --></p>

<p>在一个 PR 详情页面上，通过查看变更的&ldquo;文件&rdquo;列表即可找到&ldquo;已阅&rdquo;的功能入口。</p>

<p><!-- /wp:paragraph --><!-- wp:paragraph --></p>

<p>勾选&ldquo;已阅&rdquo;可以标记相应文件的查看状态，当选择文件&ldquo;已阅&rdquo;，则对应变更的文件将自动折叠，并在页面上告知有多少个文件&ldquo;已阅&rdquo;。</p>

<p><!-- /wp:paragraph --><!-- wp:image --></p>

<p><img alt="" height="376" src="https://oscimg.oschina.net/oscnet/up-417c7f854b6f8ad1d400dab9eab0b509a62.gif" width="700" /></p>

<p><!-- /wp:image --><!-- wp:quote --></p>

<blockquote>
<p>当 PullRequest 中的代码产生了变更，系统将默认为用户取消对应变动文件的&ldquo;已阅&rdquo;状态。</p>
</blockquote>

<p><!-- /wp:quote --><!-- wp:paragraph --></p>

<p>该功能现已在 Gitee 全面上线，如果你是仓库的所有者或管理员，现在就可以去体验这项新功能了。</p>

<p><!-- /wp:paragraph --><!-- wp:paragraph --></p>

<p>点此查看「线解决代码冲突」帮助文档：<a href="https://gitee.com/help/articles/4306">https://gitee.com/help/articles/4306</a></p>

<p><!-- /wp:paragraph --></p>
