---
layout: post
title: "代码克隆检测服务 CopyCat 已支持 Ruby/PHP 克隆检测"
---

<p>2018 年，Gitee 正式推出<span style="background-color:#ffffff; color:#4d4d4d">了 CopyCat 代码克隆检测服务。CopyCat 是 Gitee 在&nbsp;</span><a href="https://www.oschina.net/p/nicad-clone-detector">NiCad Clone Detector</a><span style="background-color:#ffffff; color:#4d4d4d">&nbsp;项目的基础上对性能进行优化和调整而推出的代码克隆检测服务，是基于&nbsp;NiCad 技术的代码克隆检测技术。CopyCat&nbsp;</span><span style="background-color:#ffffff; color:#40485b">可用于分析两个项目在代码结构上的相似度，从而识别是否存在代码抄袭的问题。</span></p>

<p><strong>目前，&nbsp;CopyCat 检测工具已经同步到 NICAD5 版本，此版本已经对 PHP/Ruby 进行了支持 。</strong></p>

<p>Ruby 检测效果图：</p>

<p><strong><img alt="" height="441" src="https://oscimg.oschina.net/oscnet/up-f9f4c2d18f90193a6f8f36e0d16cd2f0abe.png" width="700" /></strong></p>

<p>PHP检测效果图：</p>

<p><strong><img alt="" height="438" src="https://oscimg.oschina.net/oscnet/up-241842ca9cc175501be2a9a9645ac54ac4b.png" width="700" /></strong></p>

<p>经过本次更新，CopyCat 现已支持&nbsp;<span style="background-color:#ffffff; color:#40485b">Java / C / C# / PHP / Ruby 五种语言的代码克隆检测。</span></p>

<p><span style="background-color:#ffffff; color:#40485b">快来&nbsp;</span><a href="https://copycat.gitee.com/" target="_blank">https://copycat.gitee.com</a><span style="background-color:#ffffff; color:#40485b">&nbsp;体验吧！</span></p>
