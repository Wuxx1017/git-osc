---
layout: post
title: "码云企业版可独立开启 HTTPS+SSH+SVN 任意组合"
---

<p>随着使用码云的企业用户越来越多，一些企业向我们提出建议，希望允许企业关闭 HTTPS 方式访问，来避免由于企业内部开发人员因为密码过于简单，或者密码泄露导致公司项目的泄露。</p><p>而使用 SSH 方式推拉代码，虽然在初始配置上比较麻烦，但是使用过程中安全性明显高于 HTTPS，毕竟泄露密码要比泄露证书更容易。</p><p>因此我们为企业版客户增加可代码推拉的方式设置开关，默认允许通过三种方式推拉代码 SSH+HTTPS+SVN（SVN 开启后需要单独在项目管理页面再次开启 SVN 访问）。企业用户可通过设置只允许 SSH 方式来提升用户账号访问的安全性。</p><p>使用方法：企业面板 -&gt; 管理 -&gt; 安全设置。（如下图所示）</p><p><img src="https://oscimg.oschina.net/oscnet/d2912d509650265a7d6c127efe62982d26b.jpg"/></p><p>即刻前往体验：<a href="https://gitee.com/enterprises?from=news-ssh-https" target="_blank" textvalue="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>