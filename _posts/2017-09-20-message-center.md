---
layout: post
title: "消息通知全新改造"
---

- 消息通知全新改造，支持自定义接受相关类型通知，摆脱通知杂音。[详情](https://gitee.com/profile/notifications)

- 解决Gitee.com 域名下三方登录无效问题。

- git.oschina.net 域名访问跳转至 gitee.com

- 修复commit 或者路径为中文时 diff 内容中会显示文件名。

- 修复PR查看文件代码时，复制代码会把行号复制进剪切版。

- 修复OpenAPI文档测试response无值。 [OpenAPI](https://gitee.com/api/v5/swagger)