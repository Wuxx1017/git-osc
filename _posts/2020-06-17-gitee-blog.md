---
layout: post
title: "Gitee 在线解决代码冲突上线，解决冲突不再需要 Git 命令"
---

<p>许多开发者在使用 Git 的时候会遇到代码冲突的情况：当两名开发者先后往同一个分支发出合并请求，或者多个分支的代码合并到一个分支时，代码冲突就会出现，进而引起分支无法自动合并的问题。</p>

<p>这时，我们一般的解决流程是：找到冲突文件--&gt;手工修改冲突的内容--&gt;重新提交解决冲突后的文件。</p>

<p>虽然整个流程并不是特别的复杂和困难，但 Gitee 仍然想帮助大家尽量减少不必要的操作，最大化自己的工作效率，于是便推出了<strong>通过 WebIDE 在线解决代码冲突</strong>的功能。</p>

<p>Gitee 提供<strong> WebIDE 在线解决冲突</strong>的解决办法，无需客户端操作，在网页上即可完成冲突的解决：</p>

<p>1.在产生代码冲突的 PR 页面，点击「尝试通过 WebIDE 解决冲突」，进入 WebIDE。</p>

<p><img alt="" src="https://oscimg.oschina.net/oscnet/up-5233f06c1e5f8a5b5acd56eeb99278ff810.gif" width="700" /></p>

<p>2.选择相应冲突的代码文件，找到冲突的代码段，并选择合适的代码更改并接受。</p>

<p><img alt="" src="https://oscimg.oschina.net/oscnet/up-33bf2057ee055e5443d06361c436c98b54e.gif" width="700" /></p>

<p>3.「暂存」修改过的文件，随后点击「提交」将处理完冲突的代码提交到 PR 源分支，回到 PullRequest 页面，代码冲突解决。</p>

<p><img alt="" src="https://oscimg.oschina.net/oscnet/up-7da751db795f814ba975badf149ad6d1201.gif" width="700" />&nbsp;</p>

<p>通过 Gitee 的 WebIDE ，简单的三个步骤，点点鼠标即可完成冲突解决。</p>

<p>该功能现已在 Gitee 全面上线，下次遇到代码冲突时，别忘了使用 WebIDE ，帮你更高效的解决代码冲突。</p>

<p>点此查看「线解决代码冲突」帮助文档：<a href="https://gitee.com/help/articles/4305">https://gitee.com/help/articles/4305</a></p>
